#include <stdlib.h>
#include "StateNode.h"

typedef struct PairingHeapNode
{
	StateNode*                  key;
	struct  PairingHeapNode*    child;
	struct  PairingHeapNode*    sibling;
	struct  PairingHeapNode*    prev;

}PairHeap;

static PairHeap* merge_subheaps(PairHeap *p, PairHeap *q);
static PairHeap* combine_siblings(PairHeap *p);

PairHeap* PairHeap_insert(PairHeap *p, StateNode* key)
{
	PairHeap *node;

	node = (PairHeap*)malloc(sizeof(*node));
	if (node == NULL)
		return NULL;

	node->key = key;
	node->child = node->sibling = node->prev = NULL;

	if (p == NULL)
		return node;
	else
		return merge_subheaps(p, node);
}

PairHeap* PairHeap_DeleteMin(StateNode **ele, PairHeap *p)
{
	PairHeap *new_root = NULL;

	if (p == NULL)
		return NULL;
	else
	{
		*ele = p->key;
		if (p->child != NULL)
			new_root = combine_siblings(p->child);

		free(p);
	}
	return new_root;
}

static PairHeap* combine_siblings(PairHeap *p)
{
	PairHeap *tree_array[1024];
	int i, count;

	if (p->sibling == NULL)
		return p;

	for (count = 0; p != NULL; count++)
	{
		tree_array[count] = p;
		p->prev->sibling = NULL;
		p = p->sibling;
	}
	tree_array[count] = NULL;

	for (i = 1; i < count; i++)
		tree_array[i] = merge_subheaps(tree_array[i - 1], tree_array[i]);

	return tree_array[count - 1];
}

static PairHeap* merge_subheaps(PairHeap *p, PairHeap *q)
{
	if (q == NULL)
		return p;
	else if ((p->key)->cost_f <= (q->key)->cost_f)
	{
		q->prev = p;
		p->sibling = q->sibling;
		if (p->sibling != NULL)
			p->sibling->prev = p;

		q->sibling = p->child;
		if (q->sibling != NULL)
			q->sibling->prev = q;

		p->child = q;
		return p;
	}
	else
	{
		q->prev = p->prev;
		p->prev = q;
		p->sibling = q->child;
		if (p->sibling != NULL)
			p->sibling->prev = p;

		q->child = p;
		return q;
	}
}


PairHeap* h = NULL;

void enqueue(StateNode* myNode){
	h = PairHeap_insert(h, myNode);
}

StateNode* dequeue(){
	StateNode *key = NULL;
	h = PairHeap_DeleteMin(&key, h);
	return key;
}

bool isEmpty(){
	return h==NULL;
}

void clear(){
	while (!isEmpty()){
		delete dequeue();
	}
	h = NULL;
}
