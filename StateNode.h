#ifndef STATENODE_H
#define STATENODE_H

typedef struct StateNode StateNode;

struct StateNode{
	char stateRps[17];
	int cost_g;
	int cost_h;
	int cost_f;
	StateNode *parent;
	char action;
};


char* getActionString(StateNode* item){
	char* action = NULL;
	switch (item->action) {
	case 'r':
		action = "right";
		break;
	case 'l':
		action = "left";
		break;
	case 'u':
		action = "up";
		break;
	case 'd':
		action = "down";
		break;
	}
	return action;
}

#endif // STATENODE_H
