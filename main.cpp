#include <iostream>
#include <cstring>
#include <string>
//#include "PriorityQueue.h"
#include "StateNode.h"
#include <set>
#include <unordered_set>
#include <time.h>
#include <cstdlib>
#include <deque>
#include "Pairing.h"

using namespace std;

char* getActionString(StateNode* item);
void game(char* initail);
bool isSolvable(char goal[], char initial[]);
int successor(char *source, char** nextStateString, char *nextAction);
void clearList(deque<StateNode*> *list, unordered_set<string> &mylist);
int heutisticFunction(char currentStateState[]);
int calEachDistance(int index, int finalIndex);
int indexOf(char &ch);

char goal[] = "0123456789ABCDEF";

int main() {
	clock_t tStart = clock();

	char str[17];
	cin >> str;
	while (strlen(str) == 16){
		game(str);
		cout << endl;
		cin >> str;
	}
	printf("Time taken: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

	//system("pause");
	return 0;
}

void game(char* initail) {

	//Heutistic heu;
	//Heap pqueue;
	StateNode* finalState = NULL;
	StateNode* newNode = NULL;
	StateNode* myNode = new StateNode();
	int nextStateCount;
	char* nextStateString[4];
	char nextStateAction[4];

	if (!isSolvable(goal, initail)) {
		cout << "No solution!!\n";
		return;
	}
	else if (strcmp(goal, initail) == 0) {
		cout << "It is the goal state.\n";
		return;
	}

	//create queue initial 1000000
	//q = priq_new(1000000);

	strncpy(myNode->stateRps, initail, 17);
	myNode->cost_g = 0;
	myNode->cost_h = heutisticFunction(initail);
	myNode->cost_f = myNode->cost_g + myNode->cost_h;
	myNode->parent = NULL;

	// past node
	unordered_set<string> mylist;
	deque<StateNode*> list;

	enqueue(myNode);

	while (!isEmpty()) {
		//get best sate from queue
		myNode = dequeue();

		mylist.insert(myNode->stateRps);
		list.push_back(myNode);

		//check if finished state
		if (strcmp(myNode->stateRps, goal) == 0) {
			finalState = myNode;
			break;
		}

		//get next state
		nextStateCount = successor(myNode->stateRps, nextStateString, nextStateAction);
		//add in queue
		for (int i = 0; i < nextStateCount; i++) {
			//check if walk through
			if (mylist.find(nextStateString[i]) != mylist.end()){
				//walk this state jump to next
				//delete memory
				delete nextStateString[i];
				continue;
			}

			StateNode *item = new StateNode();
			strcpy(item->stateRps, nextStateString[i]);
			delete nextStateString[i];
			item->action = nextStateAction[i];
			item->cost_g = myNode->cost_g + 1;
			item->cost_h = heutisticFunction(item->stateRps);
			item->cost_f = (item->cost_g + item->cost_h);
			item->parent = myNode;
			enqueue(item);
		}
	}


	if (finalState == NULL) {
		cout << "No solution!!\n";
	}
	else {
		cout << "Solution:\n";
		string result;
		while (finalState->parent != NULL) {
			string item("move 0 to ");
			item += string(getActionString(finalState));
			item += "\n";
			result.insert(0, item);
			finalState = finalState->parent;
		}
		cout << result;
	}

	//release memory
	clear();
	clearList(&list, mylist);
}

void clearList(deque<StateNode*> *list, unordered_set<string> &mylist){
	int size = list->size();
	for (int i = 0; i < size; i++){
		delete (*list)[i];
	}
	list->clear();

	mylist.clear();
}


bool isSolvable(char goal[], char initial[]) {
	int count = 0;

	// calculate the 0's row index start from 0
	count += (strchr(initial, '0') - initial) / 4;

	for (int i = 0; i < strlen(initial); i++) {
		if (initial[i] != '0') {
			for (int j = i + 1; j < strlen(initial); j++) {
				if (initial[j] != '0' && initial[j] < initial[i])
					count++;
			}
		}
	}
	if (count % 2 == 0)
		return true;
	else
		return false;
}

int successor(char *source, char** nextStateString, char *nextAction) {
	//find where is empty position
	char *empty = strchr(source, '0');
	int emptyIndex = empty - source;
	int nextActionCount;

	//decide the successor action
	switch (emptyIndex) {
	case 0:
		nextActionCount = 2;
		nextAction[0] = 'r';
		nextAction[1] = 'd';
		break;
	case 1:
	case 2:
		nextActionCount = 3;
		nextAction[0] = 'r';
		nextAction[1] = 'd';
		nextAction[2] = 'l';
		break;
	case 3:
		nextActionCount = 2;
		nextAction[0] = 'l';
		nextAction[1] = 'd';
		break;
	case 4:
	case 8:
		nextActionCount = 3;
		nextAction[0] = 'r';
		nextAction[1] = 'd';
		nextAction[2] = 'u';
		break;
	case 5:
	case 6:
	case 9:
	case 10:
		nextActionCount = 4;
		nextAction[0] = 'r';
		nextAction[1] = 'd';
		nextAction[2] = 'u';
		nextAction[3] = 'l';
		break;
	case 7:
	case 11:
		nextActionCount = 3;
		nextAction[0] = 'l';
		nextAction[1] = 'd';
		nextAction[2] = 'u';
		break;
	case 12:
		nextActionCount = 2;
		nextAction[0] = 'r';
		nextAction[1] = 'u';
		break;
	case 13:
	case 14:
		nextActionCount = 3;
		nextAction[0] = 'r';
		nextAction[1] = 'l';
		nextAction[2] = 'u';
		break;
	case 15:
		nextActionCount = 2;
		nextAction[0] = 'l';
		nextAction[1] = 'u';
		break;
	}
	char temp;
	int i;
	for (i = 0; i < nextActionCount; i++) {
		char* tempArray = new char[17];
		strcpy(tempArray, source);
		switch (nextAction[i]) {
		case 'u':
			temp = tempArray[emptyIndex];
			tempArray[emptyIndex] = tempArray[emptyIndex - 4];
			tempArray[emptyIndex - 4] = temp;
			nextStateString[i] = tempArray;
			break;
		case 'r':
			temp = tempArray[emptyIndex];
			tempArray[emptyIndex] = tempArray[emptyIndex + 1];
			tempArray[emptyIndex + 1] = temp;
			nextStateString[i] = tempArray;
			break;
		case 'd':
			temp = tempArray[emptyIndex];
			tempArray[emptyIndex] = tempArray[emptyIndex + 4];
			tempArray[emptyIndex + 4] = temp;
			nextStateString[i] = tempArray;
			break;
		case 'l':
			temp = tempArray[emptyIndex];
			tempArray[emptyIndex] = tempArray[emptyIndex - 1];
			tempArray[emptyIndex - 1] = temp;
			nextStateString[i] = tempArray;
			break;
		}
	}
	return nextActionCount;
}

int heutisticFunction(char currentStateState[])
{
	int result = 0;

	for (int i = 0; i < strlen(currentStateState); i++)
	{
		if (currentStateState[i] != '0')
			result += calEachDistance(i, indexOf(currentStateState[i]));
	}

	return result;
}
int indexOf(char& ch)
{
	for (int i = 0; i < strlen(goal); i++)
	{
		if (goal[i] == ch)
		{
			return i;
		}
	}
	return -1;
}

int calEachDistance(int index, int finalIndex)
{
	int x = index % 4;
	int y = index / 4;

	int finalx = finalIndex % 4;
	int finaly = finalIndex / 4;

	return abs(x - finalx) + abs(y - finaly);
}
